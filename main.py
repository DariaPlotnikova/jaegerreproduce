from opentelemetry import trace
from opentelemetry.exporter import jaeger
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchExportSpanProcessor

trace.set_tracer_provider(TracerProvider())
tracer = trace.get_tracer(__name__)

jaeger_exporter = jaeger.JaegerSpanExporter(
    service_name='local-python-service',
    agent_host_name='localhost',
    agent_port=6831,
)
span_processor = BatchExportSpanProcessor(jaeger_exporter)

trace.get_tracer_provider().add_span_processor(span_processor)

with tracer.start_as_current_span('foo') as span:
    print('Hello world!')

print(span.status.status_code)  # <StatusCode.UNSET: 1>
